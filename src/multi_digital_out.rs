use embedded_hal::digital::v2::{OutputPin, StatefulOutputPin};
use tm4c123x_hal::gpio::gpioa::PAx;
use tm4c_hal::gpio::{Output, PushPull};

pub struct MultiDigitalOuput<const BUS_SIZE: usize> {
    pins: [PAx<Output<PushPull>>; BUS_SIZE],
}

impl<const BUS_SIZE: usize> MultiDigitalOuput<BUS_SIZE> {
    pub fn multi_digital_out(pins: [PAx<Output<PushPull>>; BUS_SIZE]) -> Self {
        MultiDigitalOuput { pins: pins }
    }

    pub fn get_bus_size(&self) -> usize {
        BUS_SIZE
    }

    pub fn get_max_addr(&self) -> usize {
        // bit shifting a number to the left doubles it, so this is 2.pow(BUS_SIZE)
        (1 << self.get_bus_size()) - 1
    }

    pub fn set_output(
        &mut self,
        n: usize,
    ) -> Result<(), <PAx<Output<PushPull>> as OutputPin>::Error> {
        for (index, pin) in self.pins.iter_mut().enumerate() {
            let mask = 1 << index;
            if (n & mask) > 0 {
                pin.set_high()?
            } else {
                pin.set_low()?
            }
        }

        Ok(())
    }

    pub fn get_output(&self) -> Result<usize, <PAx<Output<PushPull>> as OutputPin>::Error> {
        let mut current_output: usize = 0;
        for (index, pin) in self.pins.iter().enumerate() {
            let mask = 1 << index;
            if pin.is_set_high()? {
                current_output += mask;
            }
        }

        Ok(current_output)
    }
}
