use tm4c123x_hal::{
    bb,
    sysctl::{self, Domain, PowerState, RunMode},
};

pub fn my_init(
    power_control: &mut tm4c123x_hal::sysctl::PowerControl,
    adc: &mut tm4c123x_hal::tm4c123x::ADC0,
    p: &mut tm4c123x_hal::tm4c123x::GPIO_PORTE,
) {
    sysctl::control_power(power_control, Domain::Adc0, RunMode::Run, PowerState::On);
    sysctl::reset(power_control, Domain::Adc0);

    sysctl::control_power(power_control, Domain::GpioE, RunMode::Run, PowerState::On);
    sysctl::reset(power_control, Domain::GpioE);

    let pin_num = 3;

    unsafe {
        bb::change_bit(&p.afsel, pin_num, true);
        bb::change_bit(&p.den, pin_num, false);
        bb::change_bit(&p.amsel, pin_num, true);
    }

    unsafe {
        bb::change_bit(&adc.actss, 3, false);
    }
    adc.emux
        .modify(|r, w| unsafe { w.bits(r.bits() & !(0xF000)) });

    adc.ssmux0.write(|w| unsafe { w.bits(0) });
    unsafe {
        bb::change_bit(&adc.ssctl3, 1, true);
        bb::change_bit(&adc.ssctl3, 2, true);
        bb::change_bit(&adc.actss, 3, true);
    }
}
