#![no_main]
#![no_std]
#![allow(unused_imports)]

pub mod board;
pub mod common;
pub mod host_comm;
pub mod multi_digital_out;

use serde_json_core;

use board::Board;
use core::fmt::Write;
use embedded_hal::{prelude::_embedded_hal_serial_Write, Pwm};
use heapless::String;
use heapless::{consts::U30, Vec};
use host_comm::Packet;
use multi_digital_out::MultiDigitalOuput;

use tm4c123x_hal::{adc, gpio::GpioExt, pwm::Timer, sample_seq::SS3, serial, time::Bps};

use cortex_m_semihosting::hprintln;

#[no_mangle]
pub fn app_entry(mut board: Board) {
    let mut port_a = board.GPIO_PORTA.split(&board.power_control);
    let mut uart = serial::Serial::uart0(
        board.UART0,
        port_a.pa1.into_af_push_pull(&mut port_a.control),
        port_a.pa0.into_af_push_pull(&mut port_a.control),
        (),
        (),
        Bps(115200),
        serial::NewlineMode::Binary,
        board::clocks(),
        &board.power_control,
    );

    let mut addrs = MultiDigitalOuput::multi_digital_out([
        port_a.pa2.into_push_pull_output().downgrade(),
        port_a.pa3.into_push_pull_output().downgrade(),
        port_a.pa4.into_push_pull_output().downgrade(),
        port_a.pa5.into_push_pull_output().downgrade(),
    ]);

    addrs.set_output(0).unwrap();

    let mut blue_led_pwm = Timer::timer1(&board.power_control, board.TIMER1)
    .into_even(board.led_blue.into_af_push_pull(&mut board.portf_control));

    blue_led_pwm.set_period(4096u32);
    blue_led_pwm.set_duty((), 0);
    blue_led_pwm.enable(());

    let mut port_e = board.GPIO_PORTE.split(&board.power_control);

    let adc1 = adc::Adc::<SS3, _, _>::adc1(
        board.ADC1,
        port_e.pe4.into_ain(&mut port_e.control),
        &mut board.power_control,
    );

    loop {
        for mod_index in 0..=addrs.get_max_addr() {
            addrs.set_output(mod_index).unwrap();
            let adc_value = adc1.read_blocking();
            let packet = Packet {
                addr: mod_index as u8,
                val: adc_value as u64,
            };
            let mut s: Vec<u8, U30> = serde_json_core::to_vec(&packet).unwrap();
            s.push(b'\n').unwrap();
            // writeln!(uart, "ADDR: {} | adc_value: {} ", mod_index, adc_value).unwrap();
            // writeln!(uart, "{}", s);
            uart.write_all(&s);
            blue_led_pwm.set_duty((), adc_value.into());
            // for _ in 0..2000 {
            // cortex_m::asm::nop();
            // }
        }
    }
}
