use core::mem::{self, size_of};

use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug, PartialEq, Copy, Clone, Eq)]
pub struct Packet {
    pub addr: u8,
    pub val: u64,
}

const PACKET_SIZE: usize = size_of::<Packet>();

impl Packet {
    pub fn to_u8_vec(&self) -> [u8; PACKET_SIZE] {
        unsafe { mem::transmute_copy::<Packet, [u8; PACKET_SIZE]>(self) }
    }

    pub fn from_u8_vec(v: &[u8; PACKET_SIZE]) -> Self {
        unsafe { mem::transmute_copy::<[u8; PACKET_SIZE], Packet>(v) }
    }
}
