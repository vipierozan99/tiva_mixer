# Tiva Mixer


Just a little personal project
---

WIP: Working on fork of [tm4c-hal](https://github.com/vipierozan99/tm4c-hal). More features needed, like a good ADC abstraction.
---

Tiva Mixer is a prototype for a modular USB volume mixer for desktop, where the user can add modules with knobs and slides to the hardware then bind each module to an application and control its volume.

## Materials

![Materials](./materials.jpeg)

# CAD

![SOLIDWORKS](cad.jpeg)

# TOOLCHAIN

https://rust-embedded.github.io/book/intro/install/linux.html


# USE THIS NEXT

https://github.com/probe-rs/probe-rs





