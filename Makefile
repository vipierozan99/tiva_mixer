DEBUG_BUILD_DIR :=target/thumbv7em-none-eabihf/debug
OUTPUT := tiva_mixer

build:
	cargo build

flash: build
	arm-none-eabi-objcopy -O binary $(DEBUG_BUILD_DIR)/$(OUTPUT) $(DEBUG_BUILD_DIR)/$(OUTPUT).bin
	sudo lm4flash $(DEBUG_BUILD_DIR)/$(OUTPUT).bin

openserver: flash
	openocd -f /usr/share/openocd/scripts/board/ek-tm4c123gxl.cfg

debug:
	arm-none-eabi-gdb ./target/thumbv7em-none-eabihf/debug/tiva_mixer

# console:
# 	putty /dev/ttyACM0 -serial -sercfg 115200,8,n,1,N

uart:
	stty -F /dev/ttyACM0 115200 cs8 -cstopb -parenb igncr && cat /dev/ttyACM0

setup_toolchain:
	rustup toolchain install nightly
	rustup override set nightly
	rustup target add thumbv7em-none-eabihf